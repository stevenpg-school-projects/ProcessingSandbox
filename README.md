#Processing
Processing is a Java library that allows for easy video and audio manipulation within the Processing IDE. 

This repo contains the projects completed for CSC220, a class based on audio-visual design within the Processing environment. There are many other versions of Processing, with p5.js being the only available web-based version. The first two projects are implemented using p5.js, and can therefore be viewed on the web. The third project is written in Processing, which can be downloaded from within the repo and run in the compatible Processing IDE and Java version 8.

