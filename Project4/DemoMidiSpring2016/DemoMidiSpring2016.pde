/*******************************************************************
  DemoMidiSpring2016.pde
  
  Steven Gantz 
  4/7/2016
  Project 4
  
  Code not denoted with "# x from spec" was written by Dr. Parson.
  
  HowTo:
  1. Keys from A-Za-Z can be used to play a midi note
  2. + raises the patch up one value
  3. - lowers the patch down one value
  4. Spacebar turns off all midi notes
  5. * turns on Tremulo
  6. / turns off Tremulo
  7. UP_ARROW raises pitch
  8. DOWN_ARROW lowers pitch
  
  http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/midispec.htm
  has the MIDI spec.
*******************************************************************/

import javax.sound.midi.* ;  // Get everything in that package.
import java.util.Timer ;     // We need the Timer.
import java.util.TimerTask ;  // A way to trigger your code when the Timer goes off.

// CONFIGURATION VARIABLES -- Use these in setup() to make fixed choices.
final int midiDeviceIndex = 0 ;  // setup() checks for number of devices. Use one for output.
// NOTE: A final variable is in fact a constant that cannot be changed.

// STATE VARIABLES -- The program can change these.
volatile boolean isTimerExpired = false ;
// volatile makes changes in the Timer thread guaranteed to be visible in the
// Processing graphics thread at some time. ***NEVER*** call Processing library
// functions or interact with graphical data from another thread!!!!!!

MidiDevice.Info[] midiDeviceInfo = null ;
// See javax.sound.midi.MidiSystem and javax.sound.midi.MidiDevice

MidiDevice device = null ;
// See javax.sound.midi.MidiSystem and javax.sound.midi.MidiDevice

Receiver receiver = null ;
// javax.sound.midi.Receiver receives your OUTPUT MIDI messages (counterintuitive?)

// Global channel variable: # 3 from spec
int audioChannel = 0;

// Global patch channel: # 4 from spec
int currentPatch = 0;

// Current draw key: # 7 from spec
String drawKey = "";

// Current pitch: # 8 on spec
int currentPitch = 50;

// # 10 on spec
// Keep track of last 12 key presses
ArrayList pitchList = null;

Timer timer = null ; // from java.util.Timer

public void setup() {
  
  // # 7 from spec
  size(720, 640);
  noSmooth();
  stroke(255);
  frameRate(30);
  
  // No graphical setup in this one.
  // 1. FIND OUT WHAT MIDI DEVICES ARE AVAILABLE FOR VARIABLE midiDeviceIndex.
  midiDeviceInfo = MidiSystem.getMidiDeviceInfo();
  for (int i = 0 ; i < midiDeviceInfo.length ; i++) {
    println("MIDI DEVICE NUMBER " + i + " Name: " + midiDeviceInfo[i].getName()
      + ", Vendor: " + midiDeviceInfo[i].getVendor()
      + ", Description: " + midiDeviceInfo[i].getDescription());
  }
  // 2. OPEN ONE OF THE MIDI DEVICES UP FOR OUTPUT.
  try {
    device = MidiSystem.getMidiDevice(midiDeviceInfo[midiDeviceIndex]);
    device.open();  // Make sure to close it before this sketch terminates!!!
    // There should be a way to schedule a method when Processing closes this
    // sketch, so we can close the device there, but it is not documented for Processing 3.
    receiver = device.getReceiver();
    // NOTE: Either of the above method calls can throw MidiUnavailableException
    // if there is no available device or if it does not have a Receiver to
    // which we can send messages. The catch clause intercepts those error messages.
    
  } catch (MidiUnavailableException mx) {
    System.err.println("MIDI UNAVAILABLE"); // Error messages go here.
  }
  
  timer = new Timer(true); // isDaemon == true allows processing to terminate when this thread is running.
  // A non-Daemon thread would inhibit Processing from terminating until that thread terminates.
  TimerTask task = new MyTimerTask() ;
  timer.scheduleAtFixedRate(new MyTimerTask(), 1000L, 1000L);
  // Run MyTimerTask.run() once every 1000 msecs, starting in 1000 msecs. from now.
  
  pitchList = new ArrayList();
  for(int i = 0; i < 12; i++){
    pitchList.add(10);
  }
}

public void draw() {
  if (device == null || receiver == null || timer == null) {
    // Initialization failed, just leave the error message on the console.
    return ;
  } else if (! isTimerExpired) {
    // Not yet time to do anything.
    return ;
  }
  isTimerExpired = false ;  // reset thetimer flag
  
  background(0);
  
  // # 7 on spec
  if(currentPitch > 50){
   fill(255, 0, 0); 
  } else if(currentPitch <= 50 && currentPitch >= 32){
     fill(0, 255, 0); 
  } else if(currentPitch < 32){
     fill(0, 0, 255); 
  }
  textSize(48);
  text(drawKey, 360/2, 360/2);
  
  // # 10 on spec - Neat visuals (last part is height (in negative));
    rect(width / 12, height, -55, -(int)pitchList.get(0)*2);
    rect(width / 12 * 2, height, -55, -(int)pitchList.get(1)*2);
    rect(width / 12 * 3, height, -55, -(int)pitchList.get(2)*2);
    rect(width / 12 * 4, height, -55, -(int)pitchList.get(3)*2);
    rect(width / 12 * 5, height, -55, -(int)pitchList.get(4)*2);
    rect(width / 12 * 6, height, -55, -(int)pitchList.get(5)*2);
    rect(width / 12 * 7, height, -55, -(int)pitchList.get(6)*2);
    rect(width / 12 * 8, height, -55, -(int)pitchList.get(7)*2);
    rect(width / 12 * 9, height, -55, -(int)pitchList.get(8)*2);
    rect(width / 12 * 10, height, -55, -(int)pitchList.get(9)*2);
    rect(width / 12 * 11, height, -55, -(int)pitchList.get(10)*2);
    rect(width / 12 * 12, height, -55, -(int)pitchList.get(11)*2);
  
  if(mousePressed == true){
    line(mouseX, mouseY, pmouseX, pmouseY);
  }
}

class MyTimerTask extends TimerTask {
  // The run() method runs every time the timer goes off.
  // Only interact with the Processing thread in a VERY simple way to be safe.
  public void run() {
    isTimerExpired = true ;
  }
}

// # 4 on spec
void changePatch(boolean change){
 if(change){
    if(currentPatch >= 127){
      currentPatch = 0;
    } else {
      currentPatch++;
    }
 } else {
   if(currentPatch <= 0){
     currentPatch = 127;
   } else {
     currentPatch--;
   }
 }
}

void keyPressed() {
  println("key is " + key + ", keyCode is " + keyCode);
  
  currentPitch = 127-(byte)(key);
  
  // # 1 on spec
  if(key >= 'a' && key <= 'z'){
    pitchList.add(0, ((int)(key)));
    if(pitchList.size() == 13){
     pitchList.remove(12); 
    }
    byte offset = (byte)(key);
    drawKey = String.valueOf(key);
    int patch = currentPatch;  // There are instrument voices 0..127 in MIDI.
    int channel = audioChannel;  // There are channels (distinct instruments) 0..15.
    int pitch = currentPitch; // Pitch 0..127, where a pitch divisble by 12 is a C note.
    int volume = 127; // Volume is 0..127, where a 0 shuts off that note on that channel.
    ShortMessage patchMessage = new ShortMessage() ; // See javax.sound.ShortMessage
    try{
      patchMessage.setMessage(ShortMessage.PROGRAM_CHANGE, channel, patch, 0);
      ShortMessage noteMessage = new ShortMessage() ; // See javax.sound.ShortMessage
      noteMessage.setMessage(ShortMessage.NOTE_ON, channel, pitch, volume);
      // See also http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/midispec.htm
      receiver.send(noteMessage, -1L);  // send it now
      receiver.send(patchMessage, -1L);
    } catch(InvalidMidiDataException e){
      System.err.println("InvalidMidiDataException: " + e.getMessage());
    }
  }
  
  // # 2 on spec
  if(key >= 'A' && key <= 'Z'){
    pitchList.add(0, (int)(key));
    if(pitchList.size() == 13){
     pitchList.remove(12); 
    }
    byte offset = (byte)(key);
    drawKey = String.valueOf(key);
    int patch = currentPatch;  // There are instrument voices 0..127 in MIDI.
    int channel = audioChannel;  // There are channels (distinct instruments) 0..15.
    int pitch = currentPitch; // Pitch 0..127, where a pitch divisble by 12 is a C note.
    if(pitch > 100){
      currentPitch = 100;
    }
    int volume = 127; // Volume is 0..127, where a 0 shuts off that note on that channel.
    ShortMessage patchMessage = new ShortMessage() ; // See javax.sound.ShortMessage
    try{
      patchMessage.setMessage(ShortMessage.PROGRAM_CHANGE, channel, patch, 0);
      ShortMessage noteMessage = new ShortMessage() ; // See javax.sound.ShortMessage
      noteMessage.setMessage(ShortMessage.NOTE_ON, channel, pitch, volume);
      // See also http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/midispec.htm
      receiver.send(noteMessage, -1L);  // send it now
      receiver.send(patchMessage, -1L);
    } catch(InvalidMidiDataException e){
      System.err.println("InvalidMidiDataException: " + e.getMessage());
    }
  }
  
  // # 3 on spec
  if(key >= 48 && key <= 57){
    audioChannel = (key - 48);
  }
  
  // # 4 on spec
  if(key == '+'){
    changePatch(true);
    println("Upped patch by 1 - " + currentPatch);
  }
  
  // # 4 on spec
  if(key == '-'){
    changePatch(false);
    println("Lowered patch by 1 - " + currentPatch);
  }
  
  // # 5 on spec
  if(key == ' '){
    ShortMessage patchMessage = new ShortMessage() ; // See javax.sound.ShortMessage
    try{
      drawKey = String.valueOf(' ');
      patchMessage.setMessage(ShortMessage.PROGRAM_CHANGE, audioChannel, currentPatch, 0);
      ShortMessage noteMessage = new ShortMessage() ; // See javax.sound.ShortMessage
      for(int i = 0; i < 128; i++){
        noteMessage.setMessage(ShortMessage.NOTE_OFF, audioChannel, i, 127);
        // See also http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/midispec.htm
        receiver.send(noteMessage, -1L);  // send it now
        receiver.send(patchMessage, -1L);
      }
    } catch(InvalidMidiDataException e){
      System.err.println("InvalidMidiDataException: " + e.getMessage());
    }
  }
  
  // # 6 on spec
  if(key == '*'){
ShortMessage patchMessage = new ShortMessage() ; // See javax.sound.ShortMessage
    try{
      for(int i = 0; i < 16; i++){
        patchMessage.setMessage(ShortMessage.CONTROL_CHANGE, i, 93, 127);
        receiver.send(patchMessage, -1L);
      }
    } catch(InvalidMidiDataException e){
      System.err.println("InvalidMidiDataException: " + e.getMessage());
    }
  }
  
  // # 6 on spec
  if(key == '/'){
    ShortMessage patchMessage = new ShortMessage() ; // See javax.sound.ShortMessage
    try{
      for(int i = 0; i < 16; i++){
        patchMessage.setMessage(ShortMessage.CONTROL_CHANGE, i, 93, 0);
        receiver.send(patchMessage, -1L);
      }
    } catch(InvalidMidiDataException e){
      System.err.println("InvalidMidiDataException: " + e.getMessage());
    }
  }
  
  // # 8 on spec
  if(keyCode == UP){
     currentPitch = currentPitch + 1; 
     System.out.println("Clicked UP");
     if(currentPitch == 100){
       currentPitch = 99;
     }
  }
  
  if(keyCode == DOWN){
     currentPitch = currentPitch - 1; 
     System.out.println("Clicked Down");
     if(currentPitch == 0){
      currentPitch = 1; 
     }
  }
  
 if (key == CODED) {
  if (keyCode == DOWN) {
    // Close everything and exit on the DOWN keyboard key.
    // You may hang your synth if you do not close it.
    if (receiver != null) {
      receiver.close();
    }
    if (device != null) {
      device.close();
    }
    if (timer != null) {
      timer.cancel();
    }
    exit();  // terminate the sketch via Processing's exit(), not Java's.
  }
 }
}
      