/************************************************************
/* Author: Steven Gantz
/* Creation Date: 11/22/2015, 2/17/2016
/* Date edited by Student: 2/24/2016
/* Due Date: Friday, 3/4/2016
/* Course: CSC220 Object Oriented Multimedia Programming
/* Professor Name: Dr. Parson
/* Assignment: 1.
/* Sketch name: GeoCSC220Intro.js. This is the j5.ps translation
/*      of Processing sketch GeoCSC220Intro.
/* Purpose: Add novel 3D capabilities to this sketch.
/* 			Completed by student on 2/24/2016.
 *********************************************************/
// STUDENT: Support full screen, frozen mode, & demo mode per handout.
// STUDENT COMMENTS for tackling p5.js: See http://p5js.org/
// http://p5js.org/reference/ is the library reference, note the 3D
// primitive shapes. Unlike Processing, not all of the 2D
// primitives work in 3D mode (WEBGL being the 3rd argument to createCanvas()).
// https://github.com/processing/p5.js/wiki/Getting-started-with-WebGL-in-p5
// is a must read for working in 3D in p5.js.
var isfrozen = false;
var isdemo = false;
// These are the objects to plot.
var dealy = []; // populate with 3 below
var dealylength = 4;
var demodealy = null; // null means there is no object there yet.

var restartInterval = 60; // seconds between restarting the dealies.
var restartCounter = 0;

var isPerspective = false;

function setup() {
  // CSC220 STUDENT: We will use WEBGL to enable 3D shapes.
  /**createCanvas(1500, 750, P2D);**/
  width = 500;
  height = 500;
  createCanvas(width, height, WEBGL);

  //background(0);
  frameRate(30);
  // We need to construct dealies to populate the array.
  for (var i = 0; i < dealylength; i++) {
    dealy[i] = new GeometricDealy(0, 0,
      random(1, width / 4), random(1, height / 4),
      random(0, 256), random(0, 256), random(0, 256));
  }
}

function draw() {

  if (isPerspective) {
    perspective(60 / 180 * PI, width / height, 0.1, 50);
  } else {
    ortho(width / 2, -width / 2, -height / 2, height / 2, 0.1, 300);
  }

  rectMode(CENTER);
  /***** START trial & error DEBUG *****/
  /*****
  fill(255);
  rect(200,200,200,200);
  push();
  translate(200,200);
  fill(255,255,0);
  rect(200,200,200,200);
  pop();
  fill(0,255,255);
  rect(width/2, height/2, width/4, height/4);
  *****/
  /***** END trial & error DEBUG *****/
  // We need to support frozen mode.
  if (isfrozen) {
    return;
  }
  // We need to support the erase option.
  if (keyIsPressed && key == 'e') {
    plane(width, height);
    for (var i = 0; i < dealy.length; i++) {
      dealy[i] = new GeometricDealy(random(0, width / 14), random(0, height / 14),
        random(1, width / 6), random(1, height / 6),
        random(0, 256), random(0, 256), random(0, 256));
    }
  }
  // We need to support demo mode for your basic geometric shape.
  if (isdemo) {
    if (demodealy == null) {
      demodealy = new GeometricDealy(width / 2, height / 2,
        width / 4, height / 4, 128, 128, 128);
    }
    demodealy.displayDemo();
    return;
  }
  // STUDENT: fill a rectangle with an alpha < 255 in your background
  // color to get partial erase instead of full erase. The background
  // applies only in setup (to set initial background color) and in erase mode.

  translate(0, 0, 100);
  noFill();
  plane(width, height);

  // END STUDENT.
  demodealy = null;

  // OLD CODE START
  /**fill(0,0,0,10);
  rectMode(CENTER);
  rect(width/2, height/2, width, height);**/
  // OLD CODE END

  // This restart stuff is optional. I like restarting new dealies periodically.
  restartCounter += 1;
  if (restartCounter >= (restartInterval * frameRate)) {
    restartCounter = 0;
    //plane(width/2, height/2, 50);
  }
  for (var i = 0; i < dealy.length; i++) {
    dealy[i].display();
  }
}

// Done!
function keyTyped() {
  if (key == 'f') {
    isfrozen = !isfrozen;
    console.log("Frozen? " + isfrozen);
  }
  if (key == 'd' && !isfrozen) {
    isdemo = !isdemo;
    console.log("Demo? " + isdemo);
  }
  if (key == 'r' && !(isdemo || isfrozen)) {
    for (var i = 0; i < dealy.length; i++) {
      dealy[i] = new GeometricDealy(random(0, width / 14), random(0, height / 14),
        random(1, width / 6), random(1, height / 6),
        random(0, 256), random(0, 256), random(0, 256));
    }
  }

  // Added camera view
  if (key == 'c' & !isfrozen) {
    console.log("Camera!");
    isPerspective = !isPerspective;
  }
}

// We use initiax and initialy in the dealie
function GeometricDealy(initialx, initialy, initialwidth, initialheight,
  r, g, b) {
  this.x = initialx;
  this.y = initialy;

  // New CODE
  this.z = 1;
  // END NEW CODE

  this.squareBox = 100;

  this.w = initialwidth;
  this.h = initialheight;
  this.rr = r;
  this.gg = g;
  this.bb = b;
  this.aa = random(0, 256);
  this.speedx = (random(0, 20) - 10); // (random(-10.0, 10.1));
  if (this.speedx == 0) {
    this.speedx = 1;
  }
  this.speedy = (random(0, 20) - 10); // (random(-10.0, 10.1));
  if (this.speedy == 0) {
    this.speedy = 1;
  }
  this.speedw = 1;
  this.speedh = -1;
  this.rotation = 0.0;
  this.delta = random(-90, 90);
  this.s = .98;
  this.speedscale = random(-1, 1);

  this.display = function() {

    // Under the first push, we build our box with a cone on top
    push();
    translate(this.x, this.y, this.z);
    rotateY(frameCount * 0.05);
    rotateZ(frameCount * 0.001);
    box(this.squareBox, this.squareBox, this.squareBox);
    push();
    translate(-width / 2 + 250, -height / 2 + 25, 0);
    cone(120, 120);
    pop();
    pop();

    // Now we build our orbiting sphere!
    push();
    rotateY(frameCount * 0.05);
    rotateZ(frameCount * 0.001)
    translate(this.x + 100, this.y - 310, this.z);
    scale(this.s);
    sphere(50);
    pop();
    push();
    pop();

    // Increased mouse speed to get pull control over the dealies
    if (mouseIsPressed) {
      if (mouseX < this.x) {
        this.x = this.x - 20;
        this.z = 0;
      } else if (mouseX > this.x) {
        this.x = this.x + 20;
      }
      if (mouseY < this.y) {
        this.y = this.y - 20;
      } else if (mouseY > this.y) {
        this.y = this.y + 20;
      }
    } else {
      this.x = (this.x + this.speedx + width) % width;
      if (this.x <= 0 || this.x >= width - 1) {
        this.speedy = (random(0, 20) - 10); // (random(-10.0, 10.1));
        if (this.speedy == 0) {
          this.speedy = 1;
        }
      }
      this.y = (this.y + this.speedy);
      if (this.y > height) {
        this.y = height;
        this.speedy = -this.speedy;
        this.speedx = (random(0, 20) - 10); // (random(-10.0, 10.1));
        if (this.speedx == 0) {
          this.speedx = 1;
        }
      } else if (this.y < 0) {
        this.y = 0;
        this.speedy = -this.speedy;
        this.speedx = (random(0, 20) - 10); // (random(-10.0, 10.1));
        if (this.speedx == 0) {
          this.speedx = 1;
        }
      }
    }
    this.w += this.speedw;
    this.h += this.speedh;
    if (this.h <= 0) {
      this.h = 1;
      this.speedh = abs(this.speedh);
    } else if (this.h > height) {
      this.h = height;
      this.speedh = -abs(this.speedh);
    }
    if (this.w <= 0) {
      this.w = 1;
      this.speedw = abs(this.speedw);
    } else if (this.w > width) {
      this.w = width;
      this.speedw = -abs(this.speedw);
    }
    this.s += this.speedscale;
    if (this.s <= -2.0 || this.s >= 2.0) {
      this.s = 0.5;
      this.speedscale = ceil(random(-.01, .01));
      if (this.speedscale == 0.0) {
        this.speedscale = 1.0;
      } else if (this.speedscale >= -.05 && this.speedscale <= .05) {
        this.speedscale = .01;
      }
    }
    this.rotation += this.delta;
    if (this.rotation >= 360.0 || this.rotation < 0.0) {
      this.delta = random(-90.0, 90.0);
      while (this.rotation < 0.0) {
        this.rotation += 360.0;
      }
      while (this.rotation >= 360.0) {
        this.rotation -= 360.0;
      }
    }
    this.rr = abs(this.rr + 16) % 256;
    this.gg = abs(this.gg + 8) % 256;
    this.bb = abs(this.bb + 4) % 256;
    this.aa = abs(this.aa + 7) % 256;
  }
  this.displayDemo = function() {
    if (isdemo) {
      this.aa = 255;
      this.speedx = 0;
      this.speedy = 0;
      this.speedh = 0;
      this.speedw = 0;
      this.delta = 0;
    }
    this.display();
  }
}
