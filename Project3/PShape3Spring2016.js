/************************************************************
/* Author: Steven Gantz
/* Creation Date: 3/13/2016
/* Modification Date: 3/16/2016
/* Due Date: Wednesday, 3/30/2016
/* Course: CSC220 Object Oriented Multimedia Programming
/* Professor Name: Dr. Parson
/* Assignment: 3.
/* Sketch name: PShape3Spring2016
/* Purpose: Demonstrate 3D PShape, textures & tints.
/* NOTE: The tint function does not work on 3D images and has 
/*		 been ommitted from the project. This is as of 3/19/16
*************************************************************/
// STUDENT COMMENTS for tackling p5.js: See http://p5js.org/
// http://p5js.org/reference/ is the library reference, note the 3D
// primitive shapes. Unlike Processing, not all of the 2D
// primitives work in 3D mode (WEBGL being the 3rd argument to createCanvas()).
// https://github.com/processing/p5.js/wiki/Getting-started-with-WebGL-in-p5
// is a must read for working in 3D in p5.js.

// STUDENT: NOTE to P5 programmers:
// There is no PShape capability in P5, and furthermore, while it
// is possible to loadImage() successfully in P5, attempting to use
// the loaded image with 3D beginShape()/endShape() results in this error:
// [GroupMarkerNotSet(crbug.com/242999)!:A04A1016927F0000]GL ERROR
// :GL_INVALID_OPERATION : glDrawArrays: attempt to render with no buffer attached
// to enabled attribute 2.
// Therefore, students who want to program using P5 and aren't afraid of
// a little trial and error, are welcome to do so in 2D (not WEBGL) as
// long as they get textures & tints to work as they do in my Processing demo
// of this code. They must do all push()es and pop()s outlined below, and satisfy
// all other proejct requirements, except they may substitute 2D for 3D, and not
// use WEBGL. Getting something working with textures and tints in P5 2D can
// tale the place of 3D for P5 programmers only. You must use 3D if you do
// this assignment in Processing. I am giving you code for a working example.

var shapes = []; // populate with 4 below
var isfrozen = false;

function setup() {
  // CSC220 STUDENT: We will use WEBGL to enable 3D shapes.
  createCanvas(1500, 750, WEBGL);
  background(0);
  frameRate(60);

  img = loadImage("images/DaleParson.jpeg");

  // MyShape(
  //    myx, myy, myz,
  //    myrotx, myroty, myrotz,
  //    R, G, B,
  //    scalefactor)
  shapes[0] = new MyShape(
    width / 2, height / 3, 0,
    0, 1, 0,
    255, 255, 255,
    0.75, 2);
  shapes[1] = new MyShape(
    width / 2, height / 4 - 50, 0,
    0, 2, 0,
    255, 255, 255,
    0.5, 1);
  shapes[2] = new MyShape(
    width / 2, height / 2 + height / 4 + 100, 0,
    0, 3, 0,
    255, 255, 255,
    0.5, 1);
  shapes[3] = new MyShape(
    width / 2 + width / 4 + 50, height / 2, 0,
    2, 4, 0,
    255, 255, 255,
    0.5, 1);
}

function draw() {

  if (isfrozen) {
    return;
  }
  colorMode(RGB, 255, 255, 255, 255);
  background(0); // P5 needs this, else all-white canvas
  // move drawing origin to the top left corner per
  // https://github.com/processing/p5.js/wiki/Getting-started-with-WebGL-in-p5
  translate(-width / 2, -height / 2, 0);
  rectMode(CENTER);
  for (var i = 0; i < shapes.length; i++) {
    shapes[i].display();
    shapes[i].move();
  }
}

// We must implement the frozen command.
function keyTyped() {
  if (key == 'f') {
    isfrozen = !isfrozen;
    console.log("Frozen? " + isfrozen);
  }

}

// STUDENT: You must implement your own MyShape class that differs
// from mine in at least the following ways:
// [x]1. It must use multiple beginShape()/endShape() groupings for
//     different "body parts". They MUST differ from mine. Do not
//     use 3 rectangles that intersect in the center. Create a different
//     composite set of PShapes.
//  NOTE: P5 method MyShape.display()
//  MUST use beginShape()/endShape() to construct and render groups of vertices,
//  incurring construction and rendering costs on every call to display().
//  The P5 manual shows only 2D vertex() calls, but 3D vertex() calls appear here:
//  https://github.com/processing/p5.js/wiki/Getting-started-with-WebGL-in-p5
//  [N/A] 2. It must load an image supplied by the student and use that image to texture()
//    the shape. I used a PNG file with some semi-transparent pixel regions.
//  [N/A] 3. It must use tint() to tint the texture, tinting it different on each of
//    multiple objects of class MyShape.
//  [x] 4. Some myShape obejcts MUST use some combination of 3D translate() (including
//    a varying Z parameter) OR rotateX() OR rotateY() to achiece 3D effects. Note
//    that rotateZ() is just 2D rotate(); you may use it, but it does not fulfill
//    this requirement. This requires using P3D in Processing or WEBGL in P5.
//  [x] 5. Use translate() to position these objects.
//  [x] 6. Use scale() on at least some of these objects. Note that there are variants of
//    scale that take 2 or 3 parameters, scaling Y and/or Z differently from X.
//  [x] 7. The objects must change some of their display-oriented fields within each
//    call to move(), while keeping others constant. I did not change x, y, z, but I
//    changed rotation, and used different scales. My move() method changes only
//    3D rotations. Another possibility would be either to change Z or scale factor
//    to make the object approach/recede or grow/shrink.
//  [x] 8. Class MyShape must include data fields and a constructor to initialize them,
//    and the constructor must take some parameters. Document them.
//  [x] 9. A zero-parameter MyShape.display() method displays the MyShape object.
//     Keep freeze mode intact using code I have supplied that triggers on the 'f' key.
//     Detection of freeze mode occurs at the top of the global draw() function.
//  [x] 10. A zero-parameter MyShape.move() method changes data fields that will effect the
//    next call to display(). Think of this as an animated demo of the composite shape.
//    Mine is mildly animated. Create some sense of flow over time.
//
//
// Constructor parameters for this class give unchanging starting x,y,z coordinates,
// rotation increments for rotate[X|Y|Z](), unchanging color and scale factor.
// NOTE: rateOfChangeZ is the rate of z increase and decrease
function MyShape(myx, myy, myz, myrotx, myroty, myrotz,
  R, G, B, scalefactor, rateOfChangeZ) {
  this.x = myx;
  this.y = myy;
  this.z = myz;
  this.s = scalefactor;
  this.roc = rateOfChangeZ;
  this.rotxincr = myrotx;
  this.rotyincr = myroty;
  this.rotzincr = myrotz;
  this.rotatex = this.rotatey = this.rotatez = 0;
  this.rred = R;
  this.ggreen = G;
  this.bblue = B;

  this.grow = true;
  this.shrink = false;

  // You must create your own display() logic.
  this.display = function() {
      push(); {
        translate(this.x, this.y, this.z);
        scale(this.s);
        rotateX(radians(this.rotatex));
        rotateY(radians(this.rotatey));
        rotateZ(radians(this.rotatez));
        fill(this.rred / 2, 0, 0);
        beginShape();
        vertex(-100, -100, 0);
        vertex(100, -100, 0);
        vertex(100, 100, 0);
        endShape(CLOSE);

        // Doesn't seem to work: tint(255, 255, 255);
        texture(img);
        box(100, 100, 10);

        push(); {
          fill(this.rred, 0, 0);
          beginShape();
          vertex(100, -100, 0);
          vertex(100, 100, 0);
          vertex(300, -100, 0);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(0, 0, this.bblue / 2);
          beginShape();
          vertex(100, -100, 0);
          vertex(100, 100, 0);
          vertex(100, -100, 200);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(0, 0, this.bblue / 4);
          beginShape();
          vertex(100, -100, 0);
          vertex(100, 100, 0);
          vertex(100, -100, -200);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(this.rred / 4, 0, 0);
          beginShape();
          vertex(100, -100, 0);
          vertex(-100, -100, 0);
          vertex(100, -400, 0);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(this.rred, 0, this.bblue);
          beginShape();
          vertex(100, -100, 0);
          vertex(300, -100, 0);
          vertex(100, -400, 0);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(this.rred / 2, 0, this.bblue / 2);
          beginShape();
          vertex(100, -100, 0);
          vertex(100, -100, -200);
          vertex(100, -400, 0);
          endShape(CLOSE);
        }
        pop();
        push(); {
          fill(0, 0, this.bblue);
          beginShape();
          vertex(100, -100, 0);
          vertex(100, -100, 200);
          vertex(100, -400, 0);
          endShape(CLOSE);
        } // Happy accident, there is no pop here on purpose!!
        // It makes that weird planetary effect
      }
      pop(); // Outer level pop, back to global coordinates.
    }
    // STUDENT: read instructions for move() at start of this class.
    // You must create your own move() logic.
  this.move = function() {
    this.rotatex = (this.rotatex + this.rotxincr) % 360;
    this.rotatey = (this.rotatey + this.rotyincr) % 360;
    this.rotatez = (this.rotatez + this.rotzincr) % 360;

    if (this.grow) {
      this.z += this.roc;
      if (this.z == 150) {
        this.grow = false;
        this.shrink = true;
      }
    }
    if (this.shrink) {
      this.z -= this.roc;
      if (this.z == -500) {
        this.shrink = false;
        this.grow = true;
      }
    }
  }
}
